<!DOCTYPE HTML> 

<head>
    <meta charset="utf-8" />
    <link rel="stylesheet" href="/CSS/accueil.css" />
    <title>Cv LE PAGE Brewen</title>
</head>

<body >

    <div class="conteneurRow height100">
        <div class="coteGauchePage"></div>
        <div class="conteneurColonne backgroundBox">
            <div class="divAlignement conteneurColonne">
                <span class="titre">Connaissances</span>
                <span>Langages du web (Front et Back) : HTML, CSS, JavaScript (Jquery), PHP et SQL</span>
                <span>Langage mobile : Dart (Framework: Flutter)</span>
                <span>BDD : SQLServer, MariaDB</span>
                <span>Initiation à : Python, Symfony (Framework PHP), Java(autoditacte)</span>
                <span>
                    <div>
                        <span>Langues</span>
                        <svg xmlns="http://www.w3.org/2000/svg" width="90" height="60">
                            <rect width="90" height="60" fill="#ED2939"/>
                            <rect width="60" height="60" fill="#fff"/>
                            <rect width="30" height="60" fill="#002395"/>
                        </svg>
                        <span>(Maternelle)</span>
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 60 30" width="60" height="30">
                            <clipPath id="s">
                                <path d="M0,0 v30 h60 v-30 z"/>
                            </clipPath>
                            <clipPath id="t">
                                <path d="M30,15 h30 v15 z v15 h-30 z h-30 v-15 z v-15 h30 z"/>
                            </clipPath>
                            <g clip-path="url(#s)">
                                <path d="M0,0 v30 h60 v-30 z" fill="#012169"/>
                                <path d="M0,0 L60,30 M60,0 L0,30" stroke="#fff" stroke-width="6"/>
                                <path d="M0,0 L60,30 M60,0 L0,30" cspanp-path="url(#t)" stroke="#C8102E" stroke-width="4"/>
                                <path d="M30,0 v30 M0,15 h60" stroke="#fff" stroke-width="10"/>
                                <path d="M30,0 v30 M0,15 h60" stroke="#C8102E" stroke-width="6"/>
                            </g>
                        </svg>
                        <span>(C1)</span>
                    </div>
                </span>
            </div>
        </div>
        <div class="conteneurColonne backgroundBox">
            <div class="divAlignement conteneurColonne">
                <span class="titre" >Formations</span>
                <span>2020/2022 Bachelor informatique(Niveau Licence) - EPSI (Brest Open Campus)</span>
                <span>2018/2020 BTS Conception et Réalisation des Systèmes Automatiques (CRSA) - CFAI de Plérin</span>
                <span>2015/2018 Baccalauréat Professionnel Maintenance des Équipements Industriels - Lycée Professionnel Vauban (Brest)</span>
            </div>
        </div>
        <div class="conteneurColonne backgroundBox">
            <div class="divAlignement conteneurColonne">
                <span class="titre">Expériences Professionnelles</span>
                <span>Développeur Web Stagiaire - ICICARTEGRISE.fr</span>
                    <div>
                        <span>Février 2021 / Avril 2021 + 2 semaines en Août 2021</span>
                        <div>
                            <span>Développement de fonctionnaspantés techniques et visuelles coté cspanent et administrateur en HTML, CSS, JavaScript</span>
                            <span>Utispansation du PHP et SQL</span>
                            <span>Apprentissage de GIT</span>
                            <span>Découverte des framework BootsTrap et Jquery</span>
                            <span>Découverte de Firebase et FireStore (SGBD NoSQL en temps réel)</span>
                        </div>
                    </div>
                <span>Technicien de maintenance Alternant - Laïta</span>
                <div>
                    <span>Septembre 2018 / Août 2020</span>
                    <div>
                        <span>Interventions de maintenance préventives et curatives sur les systèmes de production automatisés</span>
                        <span>Suivi de projet de remise en fonction d'une ligne de formeuse de carton</span>
                    </div>
                </div>
            </div>
        </div>
    </div>